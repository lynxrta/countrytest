up:
	docker-compose -f docker/docker-compose.yml up -d

down:
	docker-compose -f docker/docker-compose.yml down

build:
	docker-compose -f docker/docker-compose.yml build

bash:
	docker-compose -f docker/docker-compose.yml run backend bash

init:
	docker network create landstats

restart: down up
