#!/bin/bash -e
# by Evgeniy Bondarenko <Bondarenko.Hub@gmail.com>

# Directory structure preparation
# Time Setting
export TZ=${TZ:-"UTC"}
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# PHP version
php -v
php -m

# Migrations
