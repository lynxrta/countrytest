<?php

namespace App\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use SymfonyBundles\RedisBundle\Redis\Client;

class CountryController
{
    private Client $redis;

    public function __construct(ContainerInterface $container)
    {
        $this->redis = $container->get('sb_redis.default');
    }

    function list(Request $request): JsonResponse
    {
        $members = $this->redis->executeCommand($this->redis->createCommand('smembers', ['countries']));
        $data = [];
        if (!empty($members)) {
            $rdata = $this->redis->executeCommand($this->redis->createCommand('mget', $members));
            $data = [];
            foreach($members as $ind => $member) {
                $data[$member] = $rdata[$ind];
            }
            $res = 'ok';
        } else {
            $res = 'empty';
        }

        return new JsonResponse([
            "result" => $res,
            "data" => $data
        ]);
    }

    function set(string $country): JsonResponse
    {
        $members = $this->redis->executeCommand($this->redis->createCommand('smembers', ['countries']));

        if (!in_array($country, $members)) {
            $cmd = $this->redis->createCommand('sadd', ['countries', $country]);
            $res = $this->redis->executeCommand($cmd);
        }

        if ($country) {
            $p = $this->redis->executeCommand($this->redis->createCommand('incr', [$country]));
            $res = 'ok';
        } else {
            $res = 'fail';
        }
        return new JsonResponse([
            "result" => $res
        ]);
    }
}
